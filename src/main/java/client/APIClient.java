package client;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import com.mongodb.client.MongoCollection;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.bson.Document;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;

public class APIClient {
    public static void main(String[] args)
    {
        ((LoggerContext) LoggerFactory.getILoggerFactory()).getLogger("org.mongodb.driver").setLevel(Level.ERROR);

        testRequests();
    }
    public static void testRequests()
    {
        String response;
        Document document;

        mongoDB mongoClient = new mongoDB();
        mongoClient.connectClient(mongoClient.connectionString);
        MongoCollection collection = mongoClient.connectToCollection("Crypto", "Price");

        String[] parms = new String[]{"symbol", "LTCBTC"};
        response = callRequest("https://api.binance.com/api/v3/avgPrice", parms);
        document = new Document(parms[0], parms[1])
                .append("timestamp", System.currentTimeMillis())
                .append("dateTime", LocalDateTime.now())
                .append("responseJSON", response);
        mongoClient.insertDocument(collection, document);

        parms[1] = "BTCUSDT";
        response = callRequest("https://api.binance.com/api/v3/avgPrice", parms);
        document = new Document(parms[0], parms[1])
                .append("timestamp", System.currentTimeMillis())
                .append("dateTime", LocalDateTime.now())
                .append("responseJSON", response );
        mongoClient.insertDocument(collection, document);
    }

    public static String callRequest(String _url, String[] parms)
    {
        try {
            OkHttpClient client = new OkHttpClient();
            HttpUrl.Builder urlBuilder = HttpUrl.parse(_url).newBuilder();
            if(parms.length > 0) {
                urlBuilder.addQueryParameter(parms[0], parms[1]);
            }
            String url = urlBuilder.build().toString();
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();

            return response.body().string();
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
            return "";
        }
    }
}
