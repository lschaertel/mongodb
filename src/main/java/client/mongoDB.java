package client;

import com.mongodb.BasicDBObject;
import com.mongodb.ConnectionString;
import com.mongodb.client.*;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import org.slf4j.LoggerFactory;
import org.bson.Document;

import java.util.ArrayList;


public class mongoDB {

    static ConnectionString connectionString = new ConnectionString("mongodb+srv://lschaertel:ludwig1995@testcluster-iotj1.mongodb.net/test?retryWrites=true");
    static MongoClient client;
    static MongoDatabase database;
    static MongoCollection<Document> collection;



    public static void main(String[] args)
    {
        ((LoggerContext) LoggerFactory.getILoggerFactory()).getLogger("org.mongodb.driver").setLevel(Level.ERROR);
        //Instance of class
        mongoDB localMongo = new mongoDB();

        //Connect to server
        connectClient(connectionString);
        //Connect to Collection
        connectToCollection("Database", "Customers");


        //localMongo.showAllEntries(collection); //Show all entries in table

        //Queries the Database with given parameters
        BasicDBObject query = new BasicDBObject("name", "Ludwig Schärtel")
                .append("round", "A")
                .append("name" ,"test");

        //queryCollection(collection, query, false);

        //Insert Documents - Batch for testing
        insertBatch();

        //delete documents with given query
        //deleteCollection(collection, query);
    }
    static void insertBatch()
    {
        ArrayList<Document> documents = new ArrayList<Document>();

        for(int i = 0; i <= 50; i++)
        {
            Document doc = new Document("name", "Ludwig Schärtel")
                    .append("nr", i)
                    .append("round", "A");

            documents.add(doc);
        }
        try{
            collection.insertMany(documents);
            System.out.println(String.format("%d Docuements inserted", documents.size()));
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

    }
    static void insertDocument(MongoCollection _collection, Document _document){
        _collection.insertOne(_document);
    }
    static void deleteCollection(MongoCollection _collection, BasicDBObject _query)
    {
        long counter = queryCollection(_collection, _query, true);
        if(counter > 0)
        {
            _collection.deleteMany(_query);

            System.out.println(String.format("%d Entries got deleted", counter - queryCollection(_collection, _query, true)));
        }
        else
        {
            System.out.println("There were no Entries found to be deleted.");
        }
    }
    static long queryCollection(MongoCollection _collection, BasicDBObject _query, boolean onlyCount)
    {
        FindIterable<Document> ret = _collection.find(_query);
        if(!onlyCount) {
            for (Document d : ret) {
                System.out.println(d.toJson());
            }

            if (!ret.iterator().hasNext()) {
                System.out.println("Entry not found");
            }
        }

        return _collection.countDocuments();
    }
    static MongoCollection connectToCollection(String _database, String _collection)
    {
        database =  client.getDatabase(_database);
        collection = database.getCollection(_collection);

        return collection;
    }

    static void connectClient(ConnectionString _connection)
    {
        client = MongoClients.create(_connection);
    }
}